/**
 * @file Stuff to do on startup
 */

window.PagedConfig = {
    auto: false // Disabling PagedJS auto startup
};