/* No hyphens between pages */
/* warning : may cause polyfill errors */


class noHyphenBetweenPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.hyphenToken;
  }

  afterPageLayout(pageFragment, page, breakToken) {
    
    if (pageFragment.querySelector('.pagedjs_hyphen')) {

      console.log("ho hyphens between pages")

      // find the hyphenated word  
      let block = pageFragment.querySelector('.pagedjs_hyphen');
     
      // i dont know what that line was for :thinking: i removed it
      // block.dataset.ref = this.prevHyphen;

      // move the breakToken
      let offsetMove = getFinalWord(block.innerHTML).length;

      // move the token accordingly
      page.breakToken = page.endToken.offset - offsetMove;

      // remove the last word
      block.innerHTML = block.innerHTML.replace(getFinalWord(block.innerHTML), "");

      breakToken.offset = page.endToken.offset - offsetMove;

    }
  }

}

Paged.registerHandlers(noHyphenBetweenPage);

function getFinalWord(words) {
  var n = words.split(" ");
  return n[n.length - 1];
}
