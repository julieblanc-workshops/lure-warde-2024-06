# Lure × Warde × Paged.js

Original template: [Padatrad](https://gitlab.com/editionsburnaout/padatrad) by Yann Trividic

Forked by Julie Blanc, during a workshop for Rencontres de Lure (24-25/06/2024)


## Sources

### Pad

Documentation → https://annuel2.framapad.org/p/lure-pagedjs-2024-06-a8gh


#### CSS

- CSS → https://annuel2.framapad.org/p/lure-pagedjs-2024-06-css

#### HTML

- Couverture (#cover) → https://pad.editionsburnaout.fr/p/lure-pagedjs-2024-06-cover
- Texte Warde (#txt-warde) → https://pad.editionsburnaout.fr/p/lure-pagedjs-2024-06-html
- Texte Printing office (#printing-office)→ https://pad.editionsburnaout.fr/p/lure-pagedjs-2024-06-pinter
- Texte Vanina Pinter (#txt-pinter) → https://pad.editionsburnaout.fr/p/lure-pagedjs-2024-06-pinter
- Colophon (#colophon) → https://pad.editionsburnaout.fr/p/lure-pagedjs-2024-06-credits


### View

- https://lure-x-warde.netlify.app/



### Textes 

« Le Verre en cristal ou la typographie devrait être invisible »
*The Crystal Goblet, or Printing Should Be Invisible*
Béatrice Warde, 1932

http://indexgrafik.fr/le-verre-en-cristal-ou-la-typographie-devrait-etre-invisible/


### Images

This is a printing office: https://fr.wikipedia.org/wiki/Beatrice_Warde#/media/Fichier:This_is_a_Printing_Office.jpg